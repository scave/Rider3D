//
// Created by harvenguo on 2024/2/23.
//

#include "Input.h"

NS_RIDER_BEGIN

    bool InputManager::IsKeyDown(KeyCode::Enum& code) {
        return false;
    }

    bool InputManager::IsKeyLongPress(KeyCode::Enum& code) {
        return false;
    }

    bool InputManager::IsKeyUp(KeyCode::Enum& code) {
        return false;
    }

NS_RIDER_END