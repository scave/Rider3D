#pragma once

#include <cstdint>
#include <cstddef>

namespace rider {

static const size_t MAX_COMPONENTS = 64;
typedef double TimeDelta;

}  // namespace unknown
