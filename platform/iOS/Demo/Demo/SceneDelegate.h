//
//  SceneDelegate.h
//  Demo
//
//  Created by Scave on 2023/11/11.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

