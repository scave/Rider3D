package com.rider.sample;

import com.rider.RiderEngine;

public interface IScene {
    void doInit(RiderEngine engine);
    void doUpdate();
}
